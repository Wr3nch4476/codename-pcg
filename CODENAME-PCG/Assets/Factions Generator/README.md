﻿# *Factions Generator*

## The purpose of the project

The purpose of this project is to create a procgen that can generate factions to look like it was hand crafted.

## The intended scenario for use

The intended use for this project is to have the player compete against factions in a game. Other intended scenario is to make faction generation easier while being hand crafted to the player

Game Genres intended for:
- Strategy
- RTS
- Open World

## Usage instructions

### *Project Demo:*

1) Open the Scene: **"FactionShowcase"**
2) Click on the game object that has **--- Factions** in the name
3) Enter play mode

*Note: You can use the Faction Interface script buttons or the UI buttons. WARNING Effector Buttons do NOT work if there were no faction generated*

4) Click on UI button to start the process
5) Use buttons in the following order:
**Start** -> **Generate** -> **Create UI Buttons** -> **Click on a Named button example: Lama** OR **Take Turn**

### *How to add to your own project:*
Scripts Needed:
- *Factions System Overlord*
- 2 to 5 *ClanSO* Scriptable objects

**OPTIONAL**
- *Turn Based System*
- *Faction Interface*

How to add:
1) Add the *Faction System Overlord* to the game object you want it to be on
2) Add *ClanSO* objects to the *List Faction Names*
3) Set the amount of clans that needs to be created at the *Int Clans To Make For Game*
4) Do the **Project Demo** that is shown above

**NOTE: The next few steps are optional**

5. Add the *Turn Based System* to the same game object (the same one used in step 1) or another game object in the project
6. Add the *Faction Interface* script to any game object to interact through the Unity Editor with the script



## Limitations

Currently the limitations are to only create factions within a specific rule set. 
No extra functions regarding to adding it to a specific object to be controlled by.
Factions have a limited amount of actions they can do. Can be modified in the script *Faction Stats* at after line 242, function name **DecideAction**

## Feature breakdown

**Features:**
Generates factions that can be modified or created
Semi Customizable factions


## Requirements

**Files Required to make this procgen to work:**
- Input System (minimum version 1.4)
- Easy Buttons (minimum version 1.2)
- Text Mesh Pro (minimum version 2.9)





