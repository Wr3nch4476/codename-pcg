using UnityEngine;

[CreateAssetMenu(fileName = "ClanName_", menuName = "Faction/ClanName_Preset", order = 0)]
public class ClanSO : ScriptableObject {

    public string clanName;

    public EnumLibrary.clanType clanType;

    public FactionStatsModifier clanGrowthModifier;
    
    [Space(15)]
    public EffectorModifier clanEffectorModifier;
    
    
}