using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIFactions : MonoBehaviour
{
    public static UIFactions Instance;

    [SerializeField] private TMP_Text factionStatsDisplay;

    private void Awake()
    {
        Instance = this;
    }

    public void UIAddFactionsToUI(FactionStats factionStats)
    {
        Debug.Log("Receiving Stats");
        factionStatsDisplay.text = $"{factionStats.clanName.clanName} \n Standings: {factionStats.clanStandings} \n Hostility: {factionStats.hostilityRate} \n Allegiance Chance: {factionStats.allegianceRate} \n Influence Spread: {factionStats.influenceRate} \n Expansion Rate: {factionStats.expansionRate}";

    }

    public void ClearUI()
    {
        StartCoroutine(UpdateUI());
    }


    IEnumerator UpdateUI(){       

        factionStatsDisplay.text = $"Click buttons to see stats";
        yield return new WaitForSeconds(1f);
        factionStatsDisplay.text = string.Empty;
    }
}
