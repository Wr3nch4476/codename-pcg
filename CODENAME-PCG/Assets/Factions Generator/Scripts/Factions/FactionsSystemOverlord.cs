using System.Collections;
using System.Collections.Generic;
using EasyButtons;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

public class FactionsSystemOverlord : MonoBehaviour
{
    /*
        UNITY EVENTS
    */

    /*
        Statics
    */
    public static FactionsSystemOverlord Instance;

    /*
        References
    */
    [Header("Referencs")]
    [Space(15)]
    // [SerializeField] GameObject clanPrefab;
    // [SerializeField] GameObject factionsParentRef;

    [SerializeField] GameObject buttonsRef;
    [SerializeField] Transform buttonsRefParent;

    internal void NextTurn()
    {
        for (var i = 0; i < listFactions.Count; i++)
        {
            listFactions[i].TurnHappened();
        }
    }

    /*
        Hardcore Variables
    */

    /*
        Changeable Variables
    */

    [SerializeField, Range(2, 8)] int intClansToMakeForGame = 2;

    [Space(15)]
    [Header("Testing Growth Factor Here")]
    [Space(5)]
    [SerializeField] FactionStatsModifier factionNeutralSet;
    [Space(5)]
    [SerializeField] FactionStatsModifier factionGoodSet;

    [Space(5)]
    [SerializeField] FactionStatsModifier factionBadSet;

    [Space(15)]

    /*
        Lists
    */

    [Header("Lists DEBUG HELPER")]

    [SerializeField] public List<FactionStats> listFactions = new List<FactionStats>();

    [SerializeField] List<ClanSO> listFactionNames = new List<ClanSO>();
    List<ClanSO> listUsedFactionNames = new List<ClanSO>();

    List<GameObject> listUIButtons = new List<GameObject>();

    /*

    PROGRAMING

    */

    
    public void EnterPlayMode()
    {
        UnityEditor.EditorApplication.isPlaying = true;
    }

    
    public void StartClanGeneration()
    {

#if UNITY_EDITOR
        if (!Application.isPlaying) Debug.Log($"Enter Playmode please");
#endif

        if (Application.isPlaying)
        {
            DestroyOldClans();
            Debug.Log($"This Button is generating clans");
            GenerateClans();
        }
    }

    void GenerateClans()
    {

        if (listUsedFactionNames.Count > 0)
        {

            listFactionNames.AddRange(listUsedFactionNames);
            listUsedFactionNames = new List<ClanSO>();

        }

        for (int i = 0; i < intClansToMakeForGame; i++)
        {
            //0 - 0.33: Good,0.34 - 0.67: Neutral, 0.68 - 1: Bad
            float clanType = Random.Range(0f, 1f);
            clanType = Mathf.Round(clanType * 100) / 100;

            int randomInt = Random.Range(0, listFactionNames.Count);

            ClanSO randomClan = listFactionNames[randomInt];
            listFactionNames.Remove(randomClan);
            listUsedFactionNames.Add(randomClan);

            FactionStats factionStats = new FactionStats();
            factionStats.OnEnable();

            StartCoroutine(SetClanStats(factionStats, randomClan.clanType));
            StartCoroutine(SetClanName(factionStats, randomClan));

        }

    }

    
    public void GenerateButtons()
    {
        //Destroy Old Buttons
        if (buttonsRefParent != null)
        {

            if (listUIButtons.Count >= 1)
            {
                for (int i = 0; i < listUIButtons.Count; i++)
                {
                    Destroy(listUIButtons[i].gameObject);
                }

                listUIButtons.Clear();
            }

            //Create Buttons
            Debug.Log($"{listFactions.Count} amount in the ListFactions");

            for (int i = 0; i < listFactions.Count; i++)
            {

                GameObject tempButton = Instantiate(buttonsRef, buttonsRefParent);

                FactionRefHolder tempButtonFactionHolder = tempButton.GetComponent<FactionRefHolder>();

                tempButtonFactionHolder.factionRef = listFactions[i];

                listUIButtons.Add(tempButton);

            }

        }
    }

    IEnumerator SetClanStats(FactionStats tempFactionStats, EnumLibrary.clanType clanType)
    {
        //Set Perams of the clan here
        // FactionStats tempFactionStats = instanceClan.GetComponent<FactionStats>();

        //Setting Other Stats

        switch (clanType)
        {

            case EnumLibrary.clanType.Good:

                tempFactionStats.clanMembers = Random.Range(factionGoodSet.clanMembersMin, factionGoodSet.clanMembersMax);
                tempFactionStats.clanStandings = Mathf.Round(Random.Range(factionGoodSet.clanStandingsMin, factionGoodSet.clanStandingsMax) * 100) / 100;
                tempFactionStats.hostilityRate = Mathf.Round(Random.Range(factionGoodSet.hostilityRateMin, factionGoodSet.hostilityRateMax) * 100) / 100;
                tempFactionStats.allegianceRate = Mathf.Round(Random.Range(factionGoodSet.allegianceRateMin, factionGoodSet.allegianceRateMax) * 100) / 100;
                tempFactionStats.influenceRate = Mathf.Round(Random.Range(factionGoodSet.influenceRateMin, factionGoodSet.influenceRateMax) * 100) / 100;
                tempFactionStats.expansionRate = Mathf.Round(Random.Range(factionGoodSet.expansionRateMin, factionGoodSet.expansionRateMax) * 100) / 100;

                break;

            case EnumLibrary.clanType.Bad:

                tempFactionStats.clanMembers = Random.Range(factionBadSet.clanMembersMin, factionBadSet.clanMembersMax);
                tempFactionStats.clanStandings = Mathf.Round(Random.Range(factionBadSet.clanStandingsMin, factionBadSet.clanStandingsMax) * 100) / 100;
                tempFactionStats.hostilityRate = Mathf.Round(Random.Range(factionBadSet.hostilityRateMin, factionBadSet.hostilityRateMax) * 100) / 100;
                tempFactionStats.allegianceRate = Mathf.Round(Random.Range(factionBadSet.allegianceRateMin, factionBadSet.allegianceRateMax) * 100) / 100;
                tempFactionStats.influenceRate = Mathf.Round(Random.Range(factionBadSet.influenceRateMin, factionBadSet.influenceRateMax) * 100) / 100;
                tempFactionStats.expansionRate = Mathf.Round(Random.Range(factionBadSet.expansionRateMin, factionBadSet.expansionRateMax) * 100) / 100;

                break;

            case EnumLibrary.clanType.Neutral:

                tempFactionStats.clanMembers = Random.Range(factionNeutralSet.clanMembersMin, factionNeutralSet.clanMembersMax);
                tempFactionStats.clanStandings = Mathf.Round(Random.Range(factionNeutralSet.clanStandingsMin, factionNeutralSet.clanStandingsMax) * 100) / 100;
                tempFactionStats.hostilityRate = Mathf.Round(Random.Range(factionNeutralSet.hostilityRateMin, factionNeutralSet.hostilityRateMax) * 100) / 100;
                tempFactionStats.allegianceRate = Mathf.Round(Random.Range(factionNeutralSet.allegianceRateMin, factionNeutralSet.allegianceRateMax) * 100) / 100;
                tempFactionStats.influenceRate = Mathf.Round(Random.Range(factionNeutralSet.influenceRateMin, factionNeutralSet.influenceRateMax) * 100) / 100;
                tempFactionStats.expansionRate = Mathf.Round(Random.Range(factionNeutralSet.expansionRateMin, factionNeutralSet.expansionRateMax) * 100) / 100;

                break;
        }

        //tempFactionStats.clanStandings = 0;
        //tempFactionStats.hostilityRate = 0;
        //tempFactionStats.allegianceRate = 0;
        //tempFactionStats.influenceRate = 0;
        //tempFactionStats.expansionRate = 0;

        //Rates
        //0 - 0.33 Very Very Slow
        //0.34 - 0.66 Slow
        //0.67 - 1 Moderate

        listFactions.Add(tempFactionStats);
        yield return null;
    }

    IEnumerator SetClanName(FactionStats tempFactionStats, ClanSO clanSORef)
    {

        tempFactionStats.clanName = clanSORef;

        yield return null;

    }

    void DestroyOldClans()
    {
        if (listFactions.Count >= 1)
        {
            for (int i = 0; i < listFactions.Count; i++)
            {
                listFactions[i].OnDisable();
            }

            listFactions.Clear();
        }

    }

    private void Start()
    {
        Instance = this;
        DestroyOldClans();
    }

    //SIM CODE

    public FactionStats GetRandomFaction()
    {
        int iRandomNum = Random.Range(0, listFactionNames.Count + 1);

        FactionStats randomFaction = listFactions[iRandomNum];
        return randomFaction;
    }

    public void WhoWon(FactionStats factionOne, FactionStats factionTwo)
    {
        Debug.Log($"Faction One: {factionOne.clanName} || Faction Two: {factionTwo.clanName}");
        float randomClanOne = Random.Range(0f, 10f);
        float randomClanTwo = Random.Range(0f, 10f);

        if (randomClanOne > randomClanTwo)
        {
            Debug.Log($"Faction {factionOne} has won the battle || Gain Stats");
            int random = Random.Range(0,100);
            if (random > 50)
            {
                factionOne.ApplyEffector(new Effector_StandingBoost());
            }
        }
        else if (randomClanOne < randomClanTwo)
        {
            Debug.Log($"Faction {factionTwo} has won the battle || Gain Stats");
            int random = Random.Range(0,100);
            if (random > 50)
            {
                factionTwo.ApplyEffector(new Effector_StandingBoost());
            }
            
        }
        else if (randomClanOne == randomClanTwo)
        {
            Debug.Log($"None has won the battle || NONE Gain Stats");

        }

    }

    public void ApplyInfluenceBoostToRandomFaction()
    {
        listFactions[Random.Range(0, listFactions.Count)].ApplyEffector(new Effector_InfluenceBoost());
    }

    
    public void ApplyStandingBoostToRandomFaction()
    {
        listFactions[Random.Range(0, listFactions.Count)].ApplyEffector(new Effector_StandingBoost());
    }

    
    public void ApplyAllegianceBoostToRandomFaction()
    {
        listFactions[Random.Range(0, listFactions.Count)].ApplyEffector(new Effector_AllegianceBoost());
    }

    
    public void ApplyHostilityBoostToRandomFaction()
    {
        listFactions[Random.Range(0, listFactions.Count)].ApplyEffector(new Effector_HostilityBoost());
    }

    
    public void ApplyExpansionBoostToRandomFaction()
    {
        listFactions[Random.Range(0, listFactions.Count)].ApplyEffector(new Effector_ExpansionBoost());
    }

    public void ApplyClanMemberBoostToRandomFaction()
    {
        listFactions[Random.Range(0, listFactions.Count)].ApplyEffector(new Effector_ClanMemberBoost());
    }

}