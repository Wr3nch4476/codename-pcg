using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TurnBasedSystem : MonoBehaviour {
    //UI Controller


    public static TurnBasedSystem Instance;

    //Events

    public static UnityEvent StartTurn = new UnityEvent ();

    private void Awake () {
        Instance = this;
    }

    [EasyButtons.Button]
    public void NextTurn () {
        //Game happened
        StartTurn.Invoke ();
        //Update UI here
        UIFactions.Instance.ClearUI ();

        FactionsSystemOverlord.Instance.NextTurn();
    }

}