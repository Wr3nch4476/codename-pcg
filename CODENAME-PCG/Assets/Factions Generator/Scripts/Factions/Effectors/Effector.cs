[System.Serializable]
public abstract class Effector {

    int lifeTime = 2;

    public virtual bool Apply (FactionStats factionStats, float floatModifier) {
        lifeTime--;
        if (lifeTime <= 0) {
            return false;
        }

        return true;
    }

}

