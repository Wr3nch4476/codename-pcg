public class Effector_ClanMemberBoost : Effector
{

    public override bool Apply(FactionStats factionStats, float floatModifier)
    {
        factionStats.clanMembers += (int)floatModifier;
        return base.Apply(factionStats, floatModifier);
    }

}