[System.Serializable]
public class Effector_HostilityBoost : Effector
{
    public override bool Apply(FactionStats factionStats, float floatModifier)
    {
        factionStats.hostilityRate += floatModifier;
        return base.Apply(factionStats, floatModifier);
    }

}