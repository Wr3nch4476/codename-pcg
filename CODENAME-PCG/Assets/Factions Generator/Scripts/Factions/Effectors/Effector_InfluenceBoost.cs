[System.Serializable]
public class Effector_InfluenceBoost : Effector
{
    public override bool Apply(FactionStats factionStats, float floatModifier)
    {
        factionStats.influenceRate += floatModifier;
        return base.Apply(factionStats, floatModifier);
    }

}