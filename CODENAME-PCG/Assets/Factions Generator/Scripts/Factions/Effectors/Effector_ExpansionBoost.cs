[System.Serializable]
public class Effector_ExpansionBoost : Effector
{

    public override bool Apply(FactionStats factionStats, float floatModifier)
    {
        factionStats.expansionRate += floatModifier;
        return base.Apply(factionStats, floatModifier);
    }

}


