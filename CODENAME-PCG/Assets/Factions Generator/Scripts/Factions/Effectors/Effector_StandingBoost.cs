[System.Serializable]
public class Effector_StandingBoost : Effector
{
    public override bool Apply(FactionStats factionStats, float floatModifier)
    {
        factionStats.clanStandings += floatModifier;
        return base.Apply(factionStats, floatModifier);
    }

}