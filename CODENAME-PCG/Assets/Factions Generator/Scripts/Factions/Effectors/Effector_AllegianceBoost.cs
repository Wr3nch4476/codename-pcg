[System.Serializable]
public class Effector_AllegianceBoost : Effector
{

    public override bool Apply(FactionStats factionStats, float floatModifier)
    {
        factionStats.allegianceRate += floatModifier;
        return base.Apply(factionStats, floatModifier);
    }

}