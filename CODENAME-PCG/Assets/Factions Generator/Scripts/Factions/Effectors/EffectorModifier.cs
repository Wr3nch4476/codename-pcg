using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EffectorModifier
{

    [Range(1, 10)] public int clanMembersEffector = 1;
    [Range(-5, 5)] public float standingEffector;
    [Range(-5, 5)] public float hostilityEffector;
    [Range(-5, 5)] public float allegianceEffector;
    [Range(-5, 5)] public float influenceEffector;
    [Range(-5, 5)] public float expansionEffector;
}
