using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FactionStatsModifier
{
    [Header("Standings")]
    [Range(-5, 5)] public float clanStandingsMin;
    [Range(-5, 5)] public float clanStandingsMax;

    [Header("Clan members")]
    [Range(1, 15)]public int clanMembersMin;
    [Range(1, 15)]public int clanMembersMax;

    [Header("Hostility")]
    [Range(-5, 5)] public float hostilityRateMin;
    [Range(-5, 5)] public float hostilityRateMax;

    [Header("Allegiance")]
    [Range(-5, 5)] public float allegianceRateMin;
    [Range(-5, 5)] public float allegianceRateMax;

    [Header("Influence")]
    [Range(-5, 5)] public float influenceRateMin;
    [Range(-5, 5)] public float influenceRateMax;

    [Header("Expansion")]
    [Range(-5, 5)] public float expansionRateMin;
    [Range(-5, 5)] public float expansionRateMax;
}
