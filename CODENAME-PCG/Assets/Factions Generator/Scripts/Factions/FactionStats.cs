using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class FactionStats
{
    // [Header("Unity Events")]

    // [Header("Stats")]
    public ClanSO clanName;

    public float clanStandings;
    public int clanMembers;

    public float hostilityRate;
    public float allegianceRate;
    public float influenceRate;

    public float expansionRate;

    bool factionAlive;

    List<Effector> effectors = new List<Effector>();

    public void OnEnable()
    {
        factionAlive = true;
        // TurnBasedSystem.StartTurn.AddListener(TurnHappened);
        //StartCoroutine(StartGameplay());
    }

    public void OnDisable()
    {
        factionAlive = false;
        // TurnBasedSystem.StartTurn.RemoveListener(TurnHappened);
    }

    // IEnumerator StartGameplay()
    // {
    //     yield return null;
    //     while (factionAlive)
    //     {
    //         yield return new WaitForSeconds(5f);

    //         ExpandFaction(clanName);
    //         GainTerritory();

    //     }
    //     yield return null;
    // }

    public void TurnHappened()
    {
        if (factionAlive)
        {
            ProcessEffectors();

            switch (clanName.clanType)
            {
                case EnumLibrary.clanType.Good:
                    DecideAction();

                    break;
                case EnumLibrary.clanType.Bad:
                    DecideAction();


                    break;
                case EnumLibrary.clanType.Neutral:
                    DecideAction();

                    break;
            }
        }

    }

    private void ProcessEffectors()
    {
        if (effectors.Count > 0)
        {
            for (var i = effectors.Count - 1; i >= 0; i--)
            {
                Debug.Log($"{effectors[i].ToString()}");

                if (effectors[i].ToString() == "Effector_InfluenceBoost")
                {
                    if (!effectors[i].Apply(this, clanName.clanEffectorModifier.influenceEffector))
                    {
                        Debug.Log($"Removing Influence effector from {clanName.clanName}");
                        effectors.RemoveAt(i);
                    }
                }
                else if (effectors[i].ToString() == "Effector_AllegianceBoost")
                {
                    if (!effectors[i].Apply(this, clanName.clanEffectorModifier.allegianceEffector))
                    {
                        Debug.Log($"Removing Allegiance effector from {clanName.clanName}");
                        effectors.RemoveAt(i);
                    }
                }
                else if (effectors[i].ToString() == "Effector_StandingBoost")
                {
                    if (!effectors[i].Apply(this, clanName.clanEffectorModifier.standingEffector))
                    {
                        Debug.Log($"Removing Standing effector from {clanName.clanName}");
                        effectors.RemoveAt(i);
                    }
                }
                else if (effectors[i].ToString() == "Effector_HostilityBoost")
                {
                    if (!effectors[i].Apply(this, clanName.clanEffectorModifier.hostilityEffector))
                    {
                        Debug.Log($"Removing Hostility effector from {clanName.clanName}");
                        effectors.RemoveAt(i);
                    }
                }
                else if (effectors[i].ToString() == "Effector_ClanMemberBoost")
                {
                    if (!effectors[i].Apply(this, clanName.clanEffectorModifier.clanMembersEffector))
                    {
                        Debug.Log($"Removing Clan Member effector from {clanName.clanName}");
                        effectors.RemoveAt(i);
                    }
                }
                else if (effectors[i].ToString() == "Effector_ExpansionBoost")
                {
                    if (!effectors[i].Apply(this, clanName.clanEffectorModifier.expansionEffector))
                    {
                        Debug.Log($"Removing Expansion effector from {clanName.clanName}");
                        effectors.RemoveAt(i);
                    }
                }

                /* Effector Example to apply specific modifier
                if (effectors[i].ToString() == "Effector_StandingBoost")
                {
                    if (!effectors[i].Apply(this, clanName.clanEffectorModifier.standingEffector))
                    {
                        Debug.Log($"Removing effector");
                        effectors.RemoveAt(i);
                    }
                }
                */
                // if nothing is there
                else
                {
                    Debug.Log($"Effector does not exist");
                }

            }
        }
    }

    private void GainTerritory()
    {
        FactionStats randomFactionSteal = FactionsSystemOverlord.Instance.GetRandomFaction();

        if (clanName != randomFactionSteal.clanName)
        {
            FactionsSystemOverlord.Instance.WhoWon(this, randomFactionSteal);
        }
        else if (clanName == randomFactionSteal.clanName)
        {
            int fate = Random.Range(0, 10);

            if (fate > 5)
            {
                for (int i = 0; i < 2; i++)
                {
                    Debug.Log($"Gain Double Stats");
                    ExpandFaction(clanName);
                    RoundUpStats();
                }
            }
            else
            {
                ExpandFaction(clanName);
                RoundUpStats();
            }
        }
    }

    void ExpandFaction(ClanSO clanName)
    {

        switch (clanName.clanType)
        {
            case EnumLibrary.clanType.Neutral:
                clanStandings += 0.15f;
                hostilityRate += 0.02f;
                allegianceRate += 0.01f;
                influenceRate += 0.3f;
                expansionRate += 0.4f;
                clanMembers += 2;

                break;

            case EnumLibrary.clanType.Good:
                clanStandings += 0.1f;
                hostilityRate += -0.01f;
                allegianceRate += 0.02f;
                influenceRate += 0.2f;
                expansionRate += 0.05f;
                clanMembers += 3;

                break;

            case EnumLibrary.clanType.Bad:
                clanStandings += 0.19f;
                hostilityRate += 0.08f;
                allegianceRate += -0.04f;
                influenceRate += 0.4f;
                expansionRate += 0.07f;
                clanMembers += 1;

                break;
        }

    }

    void RoundUpStats()
    {
        clanStandings = Mathf.Round(clanStandings * 100) / 100;
        hostilityRate = Mathf.Round(hostilityRate * 100) / 100;
        allegianceRate = Mathf.Round(allegianceRate * 100) / 100;
        influenceRate = Mathf.Round(influenceRate * 100) / 100;
        expansionRate = Mathf.Round(expansionRate * 100) / 100;
    }

    public void ApplyEffector(Effector effector)
    {
        Debug.Log($"Effector added to {clanName}");
        effectors.Add(effector);
        RoundUpStats();
    }


    // Can Change the way the AI or Factions behave Below
    void DecideAction()
    {
        switch (clanName.clanType)
        {
            case (EnumLibrary.clanType.Good):
                GainTerritory();
                ExpandFaction(clanName);
                break;
            case EnumLibrary.clanType.Bad:
                GainTerritory();
                GainTerritory();
                ExpandFaction(clanName);
                break;
            case (EnumLibrary.clanType.Neutral):
                ExpandFaction(clanName);
                ExpandFaction(clanName);

                break;
        }

    }
}