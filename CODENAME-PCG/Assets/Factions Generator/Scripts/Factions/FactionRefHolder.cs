using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FactionRefHolder : MonoBehaviour
{
    [SerializeField] public FactionStats factionRef;
    [SerializeField] public TMP_Text factionName;

    private void Start() {
        factionName.text = factionRef.clanName.clanName;
    }

    //Display Stats

    public void SetStatsDisplay()
    {
        
        UIFactions.Instance.UIAddFactionsToUI(factionRef);
    }


}
