namespace EnumLibrary
{

    public enum clanType
    {
        Neutral,
        Bad,
        Good
    }

    enum EffectorName
    {
        None,
        ClanMemberBoost,
        StandingBoost,
        HostilityBoost,
        AllegianceBoost,
        InfluenceBoost,
        ExpansionBoost,

    }

}