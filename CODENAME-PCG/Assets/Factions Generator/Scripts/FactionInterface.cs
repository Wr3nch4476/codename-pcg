using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactionInterface : MonoBehaviour
{
    public void InterfacePlayModeEnter()
    {
        UnityEditor.EditorApplication.isPlaying = true;
    }

    public void InterfaceStartGeneration()
    {
        FactionsSystemOverlord.Instance.StartClanGeneration();
    }

    public void InterfaceUIGenerate()
    {
        FactionsSystemOverlord.Instance.GenerateButtons();
    }

    public void InterfaceNextTurn()
    {
        TurnBasedSystem.Instance.NextTurn();
    }


    public void InterfaceEffectorInfluenceBoost()
    {
        FactionsSystemOverlord.Instance.ApplyInfluenceBoostToRandomFaction();
    }
    public void InterfaceEffectorStandingBoost()
    {
        FactionsSystemOverlord.Instance.ApplyStandingBoostToRandomFaction();
    }
    public void InterfaceEffectorAllegianceBoost()
    {
        FactionsSystemOverlord.Instance.ApplyAllegianceBoostToRandomFaction();
    }
    public void InterfaceEffectorHostilityBoost()
    {
        FactionsSystemOverlord.Instance.ApplyHostilityBoostToRandomFaction();
    }
    public void InterfaceEffectorExpansionBoost()
    {
        FactionsSystemOverlord.Instance.ApplyExpansionBoostToRandomFaction();
    }
    public void InterfaceEffectorClanMemberBoost()
    {
        FactionsSystemOverlord.Instance.ApplyClanMemberBoostToRandomFaction();
    }


}
