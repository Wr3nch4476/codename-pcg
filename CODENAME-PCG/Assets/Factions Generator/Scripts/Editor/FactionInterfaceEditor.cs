using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FactionInterface))]
public class FactionInterfaceEditor : Editor
{

    FactionInterface factionInterface;

    private void OnEnable()
    {
        factionInterface = (FactionInterface)target;
    }
    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        {
            if (GUILayout.Button("Enter Play Mode"))
            {
                factionInterface.InterfacePlayModeEnter();
            }
            if (GUILayout.Button("Start Clan Generation"))
            {
                factionInterface.InterfaceStartGeneration();
            }
            if (GUILayout.Button("Create UI Buttons"))
            {
                factionInterface.InterfaceUIGenerate();
            }
            if (GUILayout.Button("Take Turn"))
            {
                factionInterface.InterfaceNextTurn();
            }
        }
        EditorGUILayout.EndHorizontal();
        
        EditorGUILayout.Space();

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        {
            GUILayout.Label("Effectors",EditorStyles.largeLabel);
            GUILayout.Label("Effectors applies to a random Faction");

            if (GUILayout.Button("Standing Effector"))
            {
                factionInterface.InterfaceEffectorStandingBoost();
            }
            if (GUILayout.Button("Influence Effector"))
            {
                factionInterface.InterfaceEffectorInfluenceBoost();
            }
            if (GUILayout.Button("Allegiance Effector"))
            {
                factionInterface.InterfaceEffectorAllegianceBoost();
            }
            if (GUILayout.Button("Hostility Effector"))
            {
                factionInterface.InterfaceEffectorHostilityBoost();
            }
            if (GUILayout.Button("Expansion Effector"))
            {
                factionInterface.InterfaceEffectorExpansionBoost();
            }
            if (GUILayout.Button("Clan Member Effector"))
            {
                factionInterface.InterfaceEffectorClanMemberBoost();
            }
            
            
        }
        EditorGUILayout.EndHorizontal();

        serializedObject.ApplyModifiedProperties();

    }
}