using System.Collections;
using System.Collections.Generic;
using At0m1c.ObjectPooling;
using UnityEngine;

public class LineRendererObjectPool : ObjectPool<LSystemLine>
{
    public override void PutIntoObjectPool (LSystemLine opObject){
        opObject.lineRenderer.positionCount = 0;
        base.PutIntoObjectPool(opObject);
    }
}