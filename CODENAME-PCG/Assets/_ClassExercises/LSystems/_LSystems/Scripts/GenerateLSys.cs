using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GenerateLSys : MonoBehaviour
{
    [Header("References")]
    [SerializeField] Transform turtle;
    [SerializeField] LineRendererObjectPool turtlePond;

    new Camera camera;


    [Header("L-System")]
    [SerializeField] string axion;

    [SerializeField] List<Grammar> grammars = new List<Grammar>();

    [Header("Settings")]
    [SerializeField] int generations = 3;
    [SerializeField] float lineLength = 0.5f;
    [SerializeField] float rotationAngle = 30f;

    bool drawingSequence;
    [SerializeField] float stepsPerFrame = 1f;
    [SerializeField] float frameWait = 1f;

    [Header("Output")]
    [SerializeField, TextArea(3, 10)] string sequence;

    private void Start()
    {
        camera = Camera.main;
    }


    [EasyButtons.Button]
    public void Generate()
    {
        camera.orthographicSize = 10;
        transform.position = -Vector3.up * camera.orthographicSize;

        drawingSequence = true;
        StartCoroutine(GenerateSequence(() =>
        {
            StartCoroutine(DrawSequence(sequence));
        }));

    }

    public void StopGenerate()
    {
        drawingSequence = false;
    }

    IEnumerator GenerateSequence(UnityAction OnComplete)
    {
        sequence = axion;

        for (int i = 0; i < generations; i++)
        {
            sequence = ProcessSequence(sequence);
            yield return new WaitForSeconds(1f);
        }
        yield return null;
        OnComplete.Invoke();
    }

    string ProcessSequence(string sequence)
    {
        string processedSequence = string.Empty;
        char[] characters = sequence.ToCharArray();

        for (int i = 0; i < characters.Length; i++)
        {
            bool found = false;

            for (int g = 0; g < grammars.Count; g++)
            {
                if (characters[i] == grammars[g].variable)
                {
                    processedSequence += grammars[g].rule;

                    found = true;
                    break;
                }
            }

            if (!found)
            {
                processedSequence += characters[i];
            }

        }



        return processedSequence;
    }

    IEnumerator DrawSequence(string sequence)
    {
        int stepCount = 0;


        char[] characters = sequence.ToCharArray();

        turtle.localPosition = Vector3.zero;
        turtle.localEulerAngles = Vector3.zero;

        List<LSystemLine> lines = new List<LSystemLine>();
        LSystemLine line = turtlePond.GetFromObjectPool();
        lines.Add(line);

        //Stack<PosRot> posRotMemory = new 

        for (int i = 0; i < characters.Length; i++)
        {

            LSystem_Koch(characters[i]);

            line = lines[lines.Count - 1];
            line.lineRenderer.positionCount++;
            line.lineRenderer.SetPosition(line.lineRenderer.positionCount - 1, turtle.localPosition);

            //Tweak Camera
            if (turtle.localPosition.y > camera.orthographicSize * 2)
            {
                camera.orthographicSize = ((int)turtle.localPosition.y / 2);
                transform.position = new Vector3(0, -camera.orthographicSize, 0);
            }

            stepCount++;
            if (stepCount >= stepsPerFrame)
            {
                stepCount = 0;

                yield return new WaitForSeconds(frameWait);
            }

            if (!drawingSequence) break;
        }
        while (drawingSequence)
        {
            yield return null;
        }

        if (!drawingSequence && lines.Count > 0)
        {
            for (int i = 0; i < lines.Count; i++)
            {
                turtlePond.PutIntoObjectPool(lines[i]);
            }



        }

        yield return null;
    }

    private void LSystem_Koch(char _char)
    {
        switch (_char)
        {
            case 'F': //forward
                Debug.Log($"Forward");
                turtle.Translate(Vector3.up * lineLength);
                break;

            case '+': //Turn Left
                Debug.Log($"Left");
                turtle.Rotate(Vector3.forward * -rotationAngle);
                break;

            case '−': //Turn Right
            case '-': //Turn Right
                Debug.Log($"Right");
                turtle.Rotate(Vector3.forward * rotationAngle);
                break;
            default:
                Debug.Log($"Character not found {_char}");

                break;


        }
    }
}

[System.Serializable]

public class Grammar
{

    public char variable;
    public string rule;

}

[System.Serializable]

public struct PosRot
{

    public Vector3 position;
    public Quaternion rotation;

    public PosRot(Vector3 position, Quaternion rotation)
    {
        this.position = position;
        this.rotation = rotation;
    }
}