using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridGenerator : MonoBehaviour
{
    public static GridGenerator Instance;
    [SerializeField] PoolCell poolCell;

    [Header("Debug")]

    public Vector2Int gridSize;
    [SerializeField] public List<Cell> cells = new List<Cell>();

    new Camera camera;

    private void Awake()
    {
        Instance = this;

        camera = Camera.main;
    }

    void RepositionCamera()
    {
        camera.orthographicSize = gridSize.y / 1.75f;
        transform.position = new Vector3(-gridSize.x / 2, (-gridSize.y / 2) + 1, 0);
    }

    [EasyButtons.Button]
    public void GenerateGrid(int _gridSize)
    {
        this.gridSize = Vector2Int.one * _gridSize;
        ClearCells();

        for (int h = 0; h < gridSize.y; h++)
        {
            for (int w = 0; w < gridSize.x; w++)
            {
                Cell newCell = poolCell.GetFromObjectPool();
                newCell.name = $"[{w},{h}]";
                cells.Add(newCell);

                newCell.transform.localPosition = new Vector3(w, h, 0);

                //Set Start State
                newCell.SetState(false);
                //Set coordinate
                newCell.SetCoordinate(new Vector2Int(w, h));
            }
        }

        for (int i = 0; i < cells.Count; i++)
        {
            List<Cell> neighbourhood = new List<Cell>();
            int x = cells[i].coordinate.x;
            int y = cells[i].coordinate.y;

            //n
            neighbourhood.Add(cells.GetCell(new Vector2Int(x, y), gridSize));
            //ne
            neighbourhood.Add(cells.GetCell(new Vector2Int(x, y), gridSize));
            //e
            neighbourhood.Add(cells.GetCell(new Vector2Int(x, y), gridSize));
            //se
            neighbourhood.Add(cells.GetCell(new Vector2Int(x + 1, y + 1), gridSize));
            //s
            neighbourhood.Add(cells.GetCell(new Vector2Int(x, y - 1), gridSize));
            //sw
            neighbourhood.Add(cells.GetCell(new Vector2Int(x - 1, y - 1), gridSize));
            //w
            neighbourhood.Add(cells.GetCell(new Vector2Int(x - 1, y), gridSize));
            //nw
            neighbourhood.Add(cells.GetCell(new Vector2Int(x - 1, y - 1), gridSize));

            cells[i].SetNeighbourhood(neighbourhood);

        }


        RepositionCamera();
    }

    private void ClearCells()
    {
        for (int i = 0; i < cells.Count; i++)
        {
            poolCell.PutIntoObjectPool(cells[i]);
        }
        cells.Clear();
    }
}

public static class CellExtensions
{
    public static Cell GetCell(this List<Cell> cells, Vector2Int coordinate, Vector2Int gridSize)
    {
        if (coordinate.x > gridSize.x - 1) coordinate.x = 0;
        if (coordinate.x < 0) coordinate.x = gridSize.x - 1;
        if (coordinate.y > gridSize.y - 1) coordinate.y = 0;
        if (coordinate.y < 0) coordinate.y = gridSize.y - 1;

        return cells[coordinate.x + (coordinate.y * coordinate.x)];
    }

    public static Vector3 SnapPosition(this Vector3 position)
    {
        position = new Vector3
        (
            Mathf.Round(position.x),
            Mathf.Round(position.y),
            Mathf.Round(position.z)
        );

        return position;
    }
}
