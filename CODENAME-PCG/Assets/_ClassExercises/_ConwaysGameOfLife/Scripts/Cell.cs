using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
    [SerializeField] SpriteRenderer spriteRenderer;
    [SerializeField] Color colourAlive;
    [SerializeField] Color colourDead;

    public bool alive = false;

    [Header("Debug")]
    public Vector2Int coordinate;

    public List<Cell> neighborhood = new List<Cell>();

    internal void SetState(bool state)
    {
        if (alive != state) Flip();


    }

    void Flip()
    {
        alive = !alive;

        spriteRenderer.color = alive ? colourAlive : colourDead;
    }

    internal void SetCoordinate(Vector2Int coordinate)
    {
        this.coordinate = coordinate;
    }

    internal void SetNeighbourhood(List<Cell> neighbourhood)
    {
        //
    }

    internal void SetNextGeneration(bool v)
    {
        throw new NotImplementedException();
    }
}
