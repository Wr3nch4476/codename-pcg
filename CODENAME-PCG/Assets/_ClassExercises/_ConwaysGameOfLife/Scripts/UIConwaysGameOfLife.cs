using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIConwaysGameOfLife : MonoBehaviour
{
    bool simulating;

    float waitTime;

    public void StartSimulation()
    {
        ConwaysGameOfLife.Instance.StartSimulation();
    }

    public void StopSimulation()
    {
        ConwaysGameOfLife.Instance.StopSimulation();
    }

    public void ResetSimulation()
    {
        ConwaysGameOfLife.Instance.ResetSimulation();
        
    }
}
