using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Probability : MonoBehaviour
{
    [SerializeField] public List<EnemyProbabilityValue> enemyProbabilityValues = new List<EnemyProbabilityValue>();

    [Header("Debug")]
    [SerializeField] float totalProbability;

    public void RollNumber(int numberOfRolls)
    {
        enemyProbabilityValues.ForEach(x => x.rollCount = 0);
        enemyProbabilityValues.ForEach(x => x.rollCount = 0);

        for (int i = 0; i < numberOfRolls; i++)
        {
            RollNumber();
        }

        Debug.Log($"================================");
        for (int i = 0; i < enemyProbabilityValues.Count; i++)
        {
            string colour = enemyProbabilityValues[i].enemyDifficulty == EnemyDifficulty.Easy ? "green" : enemyProbabilityValues[i].enemyDifficulty == EnemyDifficulty.Medium ? "yellow" : "red";

            Debug.Log($"<color='{colour}'>{enemyProbabilityValues[i].enemyDifficulty}</color> rolled {((float)enemyProbabilityValues[i].rollCount) / numberOfRolls * 100f}%");
        }
        Debug.Log($"================================");
    }

    public void RollNumber()
    {
        totalProbability = 0;

        enemyProbabilityValues.ForEach(x =>
        {
            totalProbability += x.probability + x.probabilityLikelinessOffset;
        });

        float randomNum = Random.Range(0, totalProbability);

        EnemyProbabilityValue selectedValue = null;
        float runningTotal = 0;
        bool foundCanidate = false;

        for (int i = 0; i < enemyProbabilityValues.Count; i++)
        {
            bool offsetValue = true;
            if (!foundCanidate)
            {
                runningTotal += enemyProbabilityValues[i].probability + enemyProbabilityValues[i].probabilityLikelinessOffset;

                if (randomNum <= runningTotal)
                {
                    selectedValue = enemyProbabilityValues[i];
                    foundCanidate = true;
                    offsetValue = true;

                    enemyProbabilityValues[i].probabilityLikelinessOffset = 0;
                    enemyProbabilityValues[i].rollCount++;

                    string color = selectedValue.enemyDifficulty == EnemyDifficulty.Easy ? "green" : selectedValue.enemyDifficulty == EnemyDifficulty.Medium ? "yellow" : "red";

                    Debug.Log($"Rolled {randomNum} / {runningTotal} and found <color='{color}'> {selectedValue.enemyDifficulty} </color> enemy");
                }
            }
            if (offsetValue)
            {
                enemyProbabilityValues[i].probabilityLikelinessOffset += enemyProbabilityValues[i].probabilityIncrement;
            }
        }

    }

}

public enum EnemyDifficulty
{
    Easy,
    Medium,
    Hard
}

[System.Serializable]
public class EnemyProbabilityValue
{
    [Header("Settings")]
    public EnemyDifficulty enemyDifficulty;
    public float probability;
    public float probabilityIncrement = 1f;

    [Header("Debug")]
    public float probabilityLikelinessOffset;
    public float rollCount;
}
