using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Probability))]
public class ProbabilityEditor : Editor
{

    Probability probability;
    SerializedProperty probabilityValues;

    private void OnEnable()
    {
        probability = (Probability)target;
        probabilityValues = serializedObject.FindProperty("enemyProbabilityValues");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
        {
            if (GUILayout.Button("Roll 1"))
            {
                probability.RollNumber();
            }
            if (GUILayout.Button("Roll 500"))
            {
                probability.RollNumber(500);
            }
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);

        for (int i = 0; i < probability.enemyProbabilityValues.Count; i++)
        {
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            {
                SerializedProperty item = probabilityValues.GetArrayElementAtIndex(i);
                EditorGUILayout.PropertyField(item, GUIContent.none);

                if (GUILayout.Button("-", GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false)))
                {
                    probability.enemyProbabilityValues.RemoveAt(i);
                    i--;
                }
                EditorGUILayout.EndVertical();
                EditorGUILayout.Space();
            }
        }

        
        if (GUILayout.Button("+"))
        {
            probability.enemyProbabilityValues.Add(new EnemyProbabilityValue());
        }

        EditorGUILayout.EndVertical();

        //SerializedProperty enemyProbabilityValues = serializedObject.FindProperty("enemyProbabilityValues");
        //EditorGUILayout.PropertyField(enemyProbabilityValues, GUIContent.none);


        serializedObject.ApplyModifiedProperties();
    }
}