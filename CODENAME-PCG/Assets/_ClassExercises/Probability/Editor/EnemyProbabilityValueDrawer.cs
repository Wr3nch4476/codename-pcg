using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(EnemyProbabilityValue))]
public class EnemyProbabilityValueDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUILayout.BeginVertical(EditorStyles.helpBox);

        {
            SerializedProperty enemyDifficulty = property.FindPropertyRelative("enemyDifficulty");
            EditorGUILayout.PropertyField(enemyDifficulty, GUIContent.none);

            EditorGUILayout.BeginHorizontal();
            {
                //Colum1
                EditorGUILayout.BeginVertical();
                {
                    GUILayout.Label("Probability", EditorStyles.miniLabel);
                    SerializedProperty probability = property.FindPropertyRelative("probability");
                    EditorGUILayout.PropertyField(probability, GUIContent.none);
                }
                EditorGUILayout.EndVertical();
                //Colum 2
                EditorGUILayout.BeginVertical();
                {
                    GUILayout.Label("Probability Increments", EditorStyles.miniLabel);
                    SerializedProperty probabilityIncrement = property.FindPropertyRelative("probabilityIncrement");
                    EditorGUILayout.PropertyField(probabilityIncrement, GUIContent.none);
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            {
                //Colum1
                EditorGUILayout.BeginVertical();
                {
                    GUILayout.Label("Probability Offset", EditorStyles.miniLabel);
                    SerializedProperty probabilityLikelinessOffset = property.FindPropertyRelative("probabilityLikelinessOffset");
                    GUILayout.Label(probabilityLikelinessOffset.floatValue.ToString(), EditorStyles.miniLabel);
                }
                EditorGUILayout.EndVertical();
                //Colum 2
                EditorGUILayout.BeginVertical();
                {
                    GUILayout.Label("Total Roll Count", EditorStyles.miniLabel);
                    SerializedProperty rollCount = property.FindPropertyRelative("rollCount");
                    GUILayout.Label(rollCount.floatValue.ToString(), EditorStyles.miniLabel);
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndVertical();
        EditorGUILayout.Space();

    }
}
